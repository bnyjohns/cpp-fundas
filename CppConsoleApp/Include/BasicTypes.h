#pragma once
#include "stdafx.h"

class BasicTypes
{
public:
	static void printSize()
	{
		int a = 2; //4 bytes
		cout << "size of int is " << sizeof(a) << endl;

		short int b = 2;
		cout << "size of short int is " << sizeof(b) << endl;

		doSomeStuff(4);
	}

	static void doSomeStuff(short int a)
	{
		cout << "Inside doSomeStuff" << endl;
		cout << a << endl;
	}
};

