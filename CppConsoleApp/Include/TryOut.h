#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class Person
{
protected:
	string name;
	int age;
	int cur_id;

public:
	static int id;
	Person()
	{
		Person::id++;
		cur_id = Person::id;
	}
	virtual void getdata() = 0;
	virtual void putdata() = 0;
};

int Person::id = 0;

class Professor : public Person
{
public:
	Professor() : Person()
	{

	}
	int publications;
	void getdata() override
	{
		cin >> name >> age >> publications;
	}
	void putdata() override
	{
		cout << name << " " << age << " " << publications << " " << cur_id << endl;
	}
};

class Student : public Person
{
private:
	int marks[6];
public:
	Student() : Person()
	{

	}
	void getdata() override
	{
		cin >> name >> age;
		for (int i = 0; i < 6; i++)
		{
			cin >> marks[i];
		}
	}
	void putdata() override
	{
		cout << name << " " << age << " ";
		int sum = 0;
		for (int i = 0; i < 7; i++)
		{
			sum += marks[i];
		}
		cout << sum << " ";
		cout << cur_id << endl;
	}
};

int tryout() {

	int n, val;
	cin >> n; //The number of objects that is going to be created.
	Person *per[2];

	for (int i = 0;i < 2;i++) {

		cin >> val;
		if (val == 1) {
			// If val is 1 current object is of type Professor
			per[i] = new Professor;

		}
		else per[i] = new Student; // Else the current object is of type Student

		per[i]->getdata(); // Get the data from the user.

	}

	for (int i = 0;i<n;i++)
		per[i]->putdata(); // Print the required output for each object.

	return 0;

}
