#pragma once

/**
* This Singleton template uses a static instance creation (no "New" alloc risks)
* and saves code from all the classes willing to be used as Singletons.
* @author Virgilio (from Meyer's Singleton implementation)
*/

template <class T>
class Singleton
{
public:
	static T& Instance()
	{
		static T theSingleton;
		return theSingleton;
	}

protected:
	Singleton() {};// ctor hidden
	Singleton(Singleton const&) {}; // copy ctor hidden
	Singleton& operator=(Singleton const&) {}; // assign op. hidden
	~Singleton() {}; // dtor hidden
};


class Catalog : public Singleton<Catalog>
{
	friend class Singleton<Catalog>;
public:
	Catalog* GetCatalog()
	{
		return nullptr;
	}
};
