#pragma once

class functions
{
public:
	functions();
	~functions();
	void swapByValue(int a, int b);
	void swapByPointer(int *a, int *b);
	void swapByReference(int &a, int &b);
};

