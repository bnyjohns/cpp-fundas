#pragma once

#include <iostream>
#include "address.h"

using namespace std;

class Employee
{
	friend class EmployeeFactory; //friend class to access private member of Employee in the EmployeeFactory class
	string name;
	int age;
	Address *address;
	static int counter;
public:
	mutable string department;
	Employee(string name, int age);
	Employee(string name, int age, int houseNumber, string street);
	Employee(Employee const& e);
	void updateDepartment(string department) const;
	~Employee();
	void greet();
	virtual void sayHi() { cout << "Hi" << endl; }
};

