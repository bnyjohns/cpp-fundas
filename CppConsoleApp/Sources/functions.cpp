#include "stdafx.h"
#include "functions.h"


functions::functions()
{
}


functions::~functions()
{
}

// won't actually swap
void functions::swapByValue(int a, int b)
{
	int temp = a;
	a = b;
	b = a;
}

void functions::swapByPointer(int *a, int *b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}

void functions::swapByReference(int &a, int &b)
{
	int temp = a;
	a = b;
	b = temp;
}

inline int add(int a, int b) 
{
	//Lambda
	auto addFn = [](int c, int d) { return c + d; };
	return addFn(a, b);
}

//not lambda
auto add2(int a, int b) -> int { return a + b; }

void increment(int& a) { a++; }

void cantMutate(const int& a) {  }
