#include "stdafx.h"
#include "Address.h"
#include "Employee.h"
using namespace std;

int Employee::counter = 0;

Employee::Employee(string name, int age) : name(name), age(age)
{
	Employee::counter++;
	address = nullptr;
}

Employee::Employee(string name, int age, int houseNumber, string street)
	: Employee(name, age)
{
	if (address != nullptr)
		delete address;

	address = new Address(houseNumber, street);
}

Employee::Employee(Employee const& e) : name(e.name), age(e.age)
{
	if(e.address != nullptr)
		address = new Address(e.address->houseNumber, e.address->street);
}

Employee::~Employee()
{
	if (address != nullptr) 
	{
		delete address;
		address = nullptr;
	}
}

void Employee::greet()
{
	cout << "My name is " << name << endl;
	if (address != nullptr)
		cout << "Street name is " << address->street << endl;
}

void Employee::updateDepartment(string department) const
{
	this->department = department;
}
