// CppConsoleApp.cpp : Defines the entry point for the console application.

#include "stdafx.h"
#include "functions.h"
#include "foo.h" //StaticLibrary.lib is part of exe and doesn't need to be shipped
//#include "../DynamicLibrary/DynamicLibrary.h" //DynamicLibrary.dll has to be shipped with the exe
#include "collections.h"
#include "Employee.h"
#include "Manager.h"
#include "memory.h"
#include "templateDemo.h"
#include "BasicTypes.h"
#include "TryOut.h"
#include "StringLearn.h"

using namespace std;
using std::endl;

extern int length;
#define PI = 3;
const char NEWLINE = '\n';

//preprocessor definitions
#if _DEBUG 
void yo(){ }
#endif

#if _ABC
void sayHi() { cout << "Hi" << endl; }
#endif

//preprocessor macros
#define mul(a,b) a*b

enum class color
{
	red,
	blue,
	green
};
void main1();

int main(int argc, char* argv[])
{
	//main1();
	tryout();
	getchar();
    return 0;
}

void main1()
{
	sayHi();
	int result = mul(2, 3);
	cout << "hello world!" << endl;
	/*size_t size = sizeof(char);
	cout << size << endl;*/

	typedef int feet;
	feet distance = 10;
	int length = 5;
	color color = color::blue;

	cout << "1 + 2 from static library is " << add(1, 2) << endl;
	//cout << "2 * 3 from dynamic library is " << multiply(2, 3) << endl;

	collections collection;
	int size = 5;
	int array[5] = {};
	int *p = array;
	collection.createIntegerArray(array, size);
	collection.printArray(p, size);

	string s = "hello";
	int stringLength = s.length();

	/*int a = 10;
	int b = 20;
	functions func;
	func.swapByPointer(&a, &b);
	cout << "a is " << a << endl;
	cout << "b is " << b;*/

	Employee e1("ram", 12);
	e1.greet();

	Employee *e2 = new Employee("raj", 20);
	e2->department = "science";
	e2->greet();
	delete e2;

	Employee *e3 = new Employee("krish", 15, 20, "west avenue");
	e3->department = "dep";
	e3->greet();

	//Copy constructor. shallow copy by default. 
	//Implement copy constructor explicitly for deep copy
	Employee e4(*e3);
	e4.greet();

	//Copy assignment
	Employee e5 = *e3;
	delete e3;

	//Implicit cast to base class
	Manager m1("Kiran", 12, 4, "Taylor Av");
	Employee &e6 = m1;

	//Explicit cast of objects
	Manager &m2 = static_cast<Manager&>(e6);

	Employee *eptr = &e6;
	Manager *mptr = dynamic_cast<Manager*>(eptr);

	//memory memory;
	//no need to explicitly delete uniqueEmp or anotherSharedEmp.
	//it will be automatically taken care of(when it goes out of scope) since it is a smart pointer
	auto mem = new memory();
	auto uniqueEmp = mem->createUniqueEmployee();
	//auto anotherUniqueEmp = uniqueEmp; //compile time error
	auto sharedEmp = mem->createSharedEmployee();
	auto anotherSharedEmp = sharedEmp; //works

	Catalog::Instance().GetCatalog();

	BasicTypes::printSize();

}



