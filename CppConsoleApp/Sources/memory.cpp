#include "stdafx.h"
#include "memory.h"
#include "Employee.h"
using namespace std;

memory::memory()
{
}


memory::~memory()
{
}

void memory::demo() 
{
	string s("hello"); //in stack. will be cleared when it goes out of scope
	string* t = new string("world"); //memory allocated in heap
	delete t; //deallocate

	int *values = new int[128];
	delete[] values;
}

unique_ptr<Employee> memory::createUniqueEmployee()
{
	//auto employee = new Employee("r", 1, 3, "street");
	//return unique_ptr<Employee>(employee);
	return make_unique<Employee>("r", 1, 3, "street");
}

shared_ptr<Employee> memory::createSharedEmployee()
{
	/*auto employee = new Employee("r", 1, 3, "street");
	return shared_ptr<Employee>(employee);*/
	return make_shared<Employee>("r", 1, 3, "street");
}
