# cpp fundas

Project can be run on both MAC or Windows since the build supports CMake. Hence CMake has to be installed prior on the system.

# Steps
  - git clone https://bnyjohns@bitbucket.org/bnyjohns/cpp-fundas.git
  - cd cpp-fundas
  - cd ..
  - mkdir build
  - cd build
  - cmake -G "Visual Studio 14 2015" -A x64 ../../cpp-fundas

